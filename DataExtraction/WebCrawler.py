import urllib
from bs4 import BeautifulSoup
import re


class WebCrawler(object):
    """
     Used to propagate the label for a given set of websites through all of their pages.
     Will only find pages that start with the same URL as the seed URL.
     """
    def __init__(self, path):

        self.filepath = path

    def extract_links(self, file):
        """

        :param file: The path file containing the seed urls.
        :type file: str
        :return: The set of URLS extracted from all of the seed URLS.
        :rtype: str []
        """
        seeds = self.filepath

        f = open(seeds, 'r')

        urls = f.readlines()

        f.close()

        total_qurls = []

        for url in urls:

            stripped_url = str(url).replace("\n", "")

            headers = {
                    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) '
                                  'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'}
            um = None
            resp = None
            try:
                um = urllib.request.Request(url, headers=headers)
            except Exception as e:
                print("Could not open: " + str(url))
                print(e)

            if um is None:
                continue

            try:
                resp = urllib.request.urlopen(um)
            except Exception as e:
                print("Could not open: " + str(url))
                print(e)

            if resp is None:
                continue

            respdata = resp.read()

            soup = BeautifulSoup(respdata, "html.parser")

            qurls = []

            print("link_strings")
            for link in soup.findAll('a', attrs={'href': re.compile("^http")}):
                link_str = str(link.get('href'))
                if(link_str.find(stripped_url) != -1
                   and link_str.find("#comment") == -1
                   and link_str.find("mailto") == -1
                   and link_str.find("?share") == -1
                   and link_str.find("twitter.com") == -1
                   and link_str.find("linkedin.com") == -1
                   and link_str.find("facebook.com") == -1):
                    qurls.append(link_str)

            total_qurls.extend(qurls)

        print('total urls:')

        print(len(total_qurls))

        print(len(set(total_qurls)))

        f = open(file, 'w')

        for url in set(total_qurls):
            f.write(url + '\n')

        f.close()

        return set(total_qurls)
