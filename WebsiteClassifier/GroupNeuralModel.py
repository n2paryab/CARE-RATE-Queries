"""
Uses four keras neural networks to make predictions on the same set of data.
"""
import numpy as np
import keras


class GroupNeuralModel(object):
    """
    Uses four keras neural networks to make predictions on the same set of data.
    """

    def __init__(self, symptoms, riskfactors, daycare, treatment):
        self.symptoms = keras.models.load_model(symptoms)
        self.riskfactors = keras.models.load_model(riskfactors)
        self.daycare = keras.models.load_model(daycare)
        self.treatment = keras.models.load_model(treatment)

    def predict_classes(self, info_vecs):
        """
        :param info_vecs: Data to predict on, representing web pages.
        :type info_vecs: numpy array
        :return: numpy array of predictions for each web page, divided by category, rounded to either 0 or 1.
        :rtype: numpy array
        """
        pred1 = self.symptoms.predict_classes(info_vecs)
        pred2 = self.treatment.predict_classes(info_vecs)
        pred3 = self.daycare.predict_classes(info_vecs)
        pred4 = self.riskfactors.predict_classes(info_vecs)

        predictions = np.zeros((info_vecs.shape[0], 4))

        for entry in range(pred1.shape[0]):
            predictions[entry] = np.concatenate((pred1[entry], pred4[entry], pred3[entry], pred2[entry]), axis=0)

        return predictions

    def predict(self, info_vecs):
        """
        :param info_vecs: Data to predict on, representing web pages.
        :type info_vecs: numpy array
        :return: numpy array of predictions for each web page, divided by category, with no rounding.
        :rtype: numpy array
        """
        pred1 = self.symptoms.predict(info_vecs)
        pred2 = self.treatment.predict(info_vecs)
        pred3 = self.daycare.predict(info_vecs)
        pred4 = self.riskfactors.predict(info_vecs)

        predictions = np.zeros((info_vecs.shape[0], 4))

        for entry in range(pred1.shape[0]):
            predictions[entry] = np.concatenate((pred1[entry], pred4[entry], pred3[entry], pred2[entry]), axis=0)

        return predictions
